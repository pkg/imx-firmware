# iMX8MN DDR firmware

Firmware files downloaded from:

  https://www.nxp.com/lgfiles/NMG/MAD/YOCTO/firmware-imx-8.5.bin

## License

NXP software license agreement is detailed in the `COPYING` file.
